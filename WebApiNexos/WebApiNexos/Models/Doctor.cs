﻿using System;
using System.Collections.Generic;

namespace WebApiNexos.Models
{
    public partial class Doctor
    {
        public Doctor()
        {
            Cita = new HashSet<Cita>();
        }

        public int IdDoctor { get; set; }
        public string NombreCompleto { get; set; }
        public string Especialidad { get; set; }
        public int NumeroCredencial { get; set; }
        public string HospitalDeTrabajo { get; set; }
        public string Ciudad { get; set; }

        public virtual ICollection<Cita> Cita { get; set; }
    }
}
