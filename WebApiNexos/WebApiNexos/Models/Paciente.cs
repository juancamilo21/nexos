﻿using System;
using System.Collections.Generic;

namespace WebApiNexos.Models
{
    public partial class Paciente
    {
        public Paciente()
        {
            Cita = new HashSet<Cita>();
        }

        public int IdPaciente { get; set; }
        public string NombreCompleto { get; set; }
        public string NumeroSeguro { get; set; }
        public string CodigoPostal { get; set; }
        public string Telefono { get; set; }
        public int Cedula { get; set; }
        public string Ciudad { get; set; }

        public virtual ICollection<Cita> Cita { get; set; }
    }
}
