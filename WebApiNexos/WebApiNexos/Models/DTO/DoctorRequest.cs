﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiNexos.Models.DTO
{
    public class DoctorRequest
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdDoctor { get; set; }
        public string NombreCompleto { get; set; }
        public string Especialidad { get; set; }
        public int NumeroCredencial { get; set; }
        public string HospitalDeTrabajo { get; set; }
        public string Ciudad { get; set; }
    }
}
