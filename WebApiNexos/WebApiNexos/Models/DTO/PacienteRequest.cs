﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiNexos.Models.DTO
{
    public class PacienteRequest
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdPaciente { get; set; }
        public string NombreCompleto { get; set; }
        public string NumeroSeguro { get; set; }
        public string CodigoPostal { get; set; }
        public string Telefono { get; set; }
        public int Cedula { get; set; }
        public string Ciudad { get; set; }

    }
}
