﻿using System;
using System.Collections.Generic;

namespace WebApiNexos.Models
{
    public partial class Cita
    {
        public int Idcita { get; set; }
        public DateTime Fecha { get; set; }
        public int IdPaciente { get; set; }
        public int IdDoctor { get; set; }

        public virtual Doctor IdDoctorNavigation { get; set; }
        public virtual Paciente IdPacienteNavigation { get; set; }
    }
}
