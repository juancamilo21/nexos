﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApiNexos.Migrations
{
    public partial class MigracionInicial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Doctor",
                columns: table => new
                {
                    idDoctor = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    nombreCompleto = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    especialidad = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    numeroCredencial = table.Column<int>(nullable: false),
                    hospitalDeTrabajo = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    ciudad = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Doctor", x => x.idDoctor);
                });

            migrationBuilder.CreateTable(
                name: "Paciente",
                columns: table => new
                {
                    idPaciente = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    nombreCompleto = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    numeroSeguro = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    codigoPostal = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    telefono = table.Column<string>(unicode: false, maxLength: 11, nullable: false),
                    cedula = table.Column<int>(nullable: false),
                    ciudad = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Paciente", x => x.idPaciente);
                });

            migrationBuilder.CreateTable(
                name: "Cita",
                columns: table => new
                {
                    idcita = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    fecha = table.Column<DateTime>(type: "date", nullable: false),
                    idPaciente = table.Column<int>(nullable: false),
                    idDoctor = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cita", x => x.idcita);
                    table.ForeignKey(
                        name: "FK_Cita_Doctor",
                        column: x => x.idDoctor,
                        principalTable: "Doctor",
                        principalColumn: "idDoctor",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Cita_Paciente",
                        column: x => x.idPaciente,
                        principalTable: "Paciente",
                        principalColumn: "idPaciente",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cita_idDoctor",
                table: "Cita",
                column: "idDoctor");

            migrationBuilder.CreateIndex(
                name: "IX_Cita_idPaciente",
                table: "Cita",
                column: "idPaciente");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Cita");

            migrationBuilder.DropTable(
                name: "Doctor");

            migrationBuilder.DropTable(
                name: "Paciente");
        }
    }
}
