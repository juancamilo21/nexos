﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApiNexos.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DoctorController : ControllerBase
    {
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            using (Models.NexosContext db = new Models.NexosContext())
            {
                var lst = (from d in db.Doctor
                           select d).ToList();

                return Ok(lst);
            }

        }

        [HttpPost]
        public ActionResult Post([FromBody] Models.DTO.DoctorRequest model)
        {
            try
            {
                using (Models.NexosContext db = new Models.NexosContext())
                {
                    Models.Doctor oDoctor = new Models.Doctor();
                    oDoctor.NombreCompleto = model.NombreCompleto;
                    oDoctor.Especialidad = model.Especialidad;
                    oDoctor.NumeroCredencial = model.NumeroCredencial;
                    oDoctor.HospitalDeTrabajo = model.HospitalDeTrabajo;
                    oDoctor.Ciudad = model.Ciudad;
                    db.Doctor.Add(oDoctor);
                    db.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(ex.ToString(), "Error al crear el doctor");

            }
            return Ok();
        }

       
    }
}