﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApiNexos.Models;

namespace WebApiNexos.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PacienteController : ControllerBase
    {

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            using (Models.NexosContext db = new Models.NexosContext())
            {
                var lst = (from d in db.Paciente 
                           select d).ToList();

                return Ok(lst);
            }

        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<string>> Get(int id)
        {
            using (Models.NexosContext db = new Models.NexosContext())
            {
                var lst = (from d in db.Paciente where d.IdPaciente == id
                           select d).ToList();

                return Ok(lst);
            }

        }



        [HttpPost]
        public ActionResult Post( Models.DTO.PacienteRequest model)
        {
            try
            {
                using (Models.NexosContext db = new Models.NexosContext())
                {
                    Models.Paciente oPaciente = new Models.Paciente();
                    oPaciente.NombreCompleto = model.NombreCompleto;
                    oPaciente.NumeroSeguro = model.NumeroSeguro;
                    oPaciente.CodigoPostal = model.CodigoPostal;
                    oPaciente.Telefono = model.Telefono;
                    oPaciente.Cedula = model.Cedula;
                    oPaciente.Ciudad = model.Ciudad;
                    db.Paciente.Add(oPaciente);
                    db.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(ex.ToString(), "Error al Crear al paciente");

            }
           

            return Ok();
        }

        [HttpPut]
        public ActionResult Put( Models.DTO.PacienteRequest model)
        {
            try
            {
                using (Models.NexosContext db = new Models.NexosContext())
                {
                    Models.Paciente oPaciente = db.Paciente.Find(model.IdPaciente);
                    oPaciente.NombreCompleto = model.NombreCompleto;
                    oPaciente.NumeroSeguro = model.NumeroSeguro;
                    oPaciente.CodigoPostal = model.CodigoPostal;
                    oPaciente.Telefono = model.Telefono;
                    oPaciente.Cedula = model.Cedula;
                    oPaciente.Ciudad = model.Ciudad;
                    db.Entry(oPaciente).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    db.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(ex.ToString(), "Error al Editar al paciente");

            }
            

            return Ok();
        }

        [HttpDelete]
        public ActionResult Delete( Models.DTO.PacienteRequest model)
        {
            try
            {
                using (Models.NexosContext db = new Models.NexosContext())
                {
                    Models.Paciente oPaciente = db.Paciente.Find(model.IdPaciente);
                    db.Paciente.Remove(oPaciente);
                    db.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(ex.ToString(), "Error al eliminar el paciente");

            }


            return Ok();
        }
    }
}