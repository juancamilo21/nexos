﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApiNexos.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CitaController : ControllerBase
    {
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            using (Models.NexosContext db = new Models.NexosContext())
            {
                var lst = (from d in db.Cita
                           select d).ToList();

                return Ok(lst);
            }

        }

        [HttpPost]
        public ActionResult Post([FromBody] Models.DTO.CitaRequest model)
        {
            try
            {
                using (Models.NexosContext db = new Models.NexosContext())
                {
                    Models.Cita oCita = new Models.Cita();
                    oCita.Fecha = model.Fecha;
                    oCita.IdDoctor = model.IdDoctor;
                    oCita.IdPaciente = model.IdPaciente;
                    db.Cita.Add(oCita);
                    db.SaveChanges();

                }
                
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(ex.ToString(),"Error al crear la cita");
                
            }
            return Ok();


        }
        [HttpPut]
        public ActionResult Put([FromBody] Models.DTO.CitaRequest model)
        {
            try
            {
                using (Models.NexosContext db = new Models.NexosContext())
                {
                    Models.Cita oCita = db.Cita.Find(model.IdCita);
                    oCita.Fecha = model.Fecha;
                    oCita.IdDoctor = model.IdDoctor;
                    oCita.IdPaciente = model.IdPaciente;
                    db.Entry(oCita).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    db.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(ex.ToString(), "Error al Editar la cita");

            }


            return Ok();
        }

        [HttpDelete]
        public ActionResult Delete([FromBody] Models.DTO.CitaRequest model)
        {
            try
            {
                using (Models.NexosContext db = new Models.NexosContext())
                {
                    Models.Cita oCita = db.Cita.Find(model.IdCita);
                    db.Cita.Remove(oCita);
                    db.SaveChanges();

                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError(ex.ToString(), "Error al Eliminar la cita");

            }


            return Ok();
        }


    }
}